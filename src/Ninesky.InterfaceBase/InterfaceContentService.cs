﻿/*======================================
 作者：洞庭夕照
 创建：2017.04.19
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 论坛：bbs.ninesky.cn
 版本：v1.0.0.0
 =====================================*/
using Ninesky.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.InterfaceBase
{
    /// <summary>
    /// 内容接口
    /// </summary>
    public interface InterfaceContentService
    {
        /// <summary>
        /// 查找内容列表
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="inputer">录入者</param>
        /// <param name="contentId">内容Id</param>
        /// <param name="categoryId">栏目Id</param>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="orderType">排序</param>
        /// <returns></returns>
        Task<Paging<Content>> FindListAsync(ContentStatus? status,string title, string inputer, int contentId = 0, int categoryId = 0, int pageIndex = 1, int pageSize = 20, ContentOrder orderType = ContentOrder.IdDesc);
    }
}
