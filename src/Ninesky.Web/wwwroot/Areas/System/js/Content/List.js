﻿$(document).ready(function () {
    //标签切换
    $('#contentab').on('shown.bs.tab', function (e) {
        switch ($(e.target).text())
        {
            case "草稿":
                alert("dd");
                break;
        }
    })
    //表格
    $('#contenttable').bootstrapTable({
        pagination: true,
        sidePagination: 'server',
        pageList: [10, 20, 50, 100],
        search: true,
        columns: [{
            checkbox: true,
            field: 'contentId',
            title: '序号'
        }, {
                field: 'title',
            title: '标题',
            formatter: function (value, row, index) {
                return '<a href="' + $('#contenttable').attr("data-ns-url") + '/' + row.contentId + '">' + value + '</a>';
            }
        }, {
            field: 'inputer',
            title: '录入人'
        }, {
                field: 'hits',
            title: '点击数'
        }, {
                field: 'createTime',
            title: '创建时间'
        }, {
                field: 'status',
            title: '状态'
        }]
    });
})