﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Ninesky.Web;
using Ninesky.Models;

namespace Ninesky.Web.Migrations
{
    [DbContext(typeof(NineskyDbContext))]
    [Migration("20170423151609_content")]
    partial class content
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Ninesky.Models.Article", b =>
                {
                    b.Property<int>("ArticleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ArticleContent");

                    b.Property<string>("Author")
                        .HasMaxLength(255);

                    b.Property<int>("ContentId");

                    b.Property<string>("Intro")
                        .HasMaxLength(255);

                    b.Property<string>("Keyword")
                        .HasMaxLength(255);

                    b.Property<string>("Source")
                        .HasMaxLength(255);

                    b.HasKey("ArticleId");

                    b.HasIndex("ContentId");

                    b.ToTable("Articles");
                });

            modelBuilder.Entity("Ninesky.Models.Category", b =>
                {
                    b.Property<int>("CategoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ChildNum");

                    b.Property<int>("ContentModuleId");

                    b.Property<int>("ContentOrder");

                    b.Property<string>("ContentView")
                        .HasMaxLength(200);

                    b.Property<string>("Creater")
                        .HasMaxLength(255);

                    b.Property<int>("Depth");

                    b.Property<string>("Description");

                    b.Property<string>("LinkUrl")
                        .HasMaxLength(500);

                    b.Property<string>("Meta_Description")
                        .HasMaxLength(255);

                    b.Property<string>("Meta_Keywords")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("Order");

                    b.Property<int>("PageSize");

                    b.Property<int>("ParentId");

                    b.Property<string>("ParentPath");

                    b.Property<string>("PicUrl")
                        .HasMaxLength(255);

                    b.Property<bool>("ShowOnMenu");

                    b.Property<int>("Target");

                    b.Property<int>("Type");

                    b.Property<string>("View")
                        .HasMaxLength(255);

                    b.HasKey("CategoryId");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("Ninesky.Models.Content", b =>
                {
                    b.Property<int>("ContentId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("DefaultPicUrl")
                        .HasMaxLength(255);

                    b.Property<int>("Hits");

                    b.Property<string>("Inputer");

                    b.Property<string>("LinkUrl")
                        .HasMaxLength(255);

                    b.Property<int>("Status");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("ContentId");

                    b.ToTable("Contents");
                });

            modelBuilder.Entity("Ninesky.Models.Module", b =>
                {
                    b.Property<int>("ModuleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Controller")
                        .HasMaxLength(50);

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<bool>("Enabled");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("ModuleId");

                    b.ToTable("Modules");
                });

            modelBuilder.Entity("Ninesky.Models.Article", b =>
                {
                    b.HasOne("Ninesky.Models.Content", "Content")
                        .WithMany()
                        .HasForeignKey("ContentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
