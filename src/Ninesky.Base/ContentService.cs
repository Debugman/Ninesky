﻿/*======================================
 作者：洞庭夕照
 创建：2016.12.19
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 版本：v1.0.0.0
 =====================================*/
using Microsoft.EntityFrameworkCore;
using Ninesky.InterfaceBase;
using Ninesky.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.Base
{
    /// <summary>
    /// 栏目服务类
    /// </summary>
    public class ContentService : InterfaceContentService
    {
        private InterfaceBaseService<Content> _baseService;
        public ContentService(DbContext dbContext)
        {
            _baseService = new BaseService<Content>(dbContext);
        }

        /// <summary>
        /// 查找内容列表
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="inputer">录入者</param>
        /// <param name="contentId">内容Id</param>
        /// <param name="categoryId">栏目Id</param>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="orderType">排序</param>
        /// <returns></returns>
        public async Task<Paging<Content>> FindListAsync(ContentStatus? status, string title, string inputer, int contentId = 0, int categoryId = 0, int pageIndex = 1, int pageSize = 20, ContentOrder orderType = ContentOrder.IdDesc)
        {
            Paging<Content> paging = new Paging<Content>() { PageIndex = pageIndex, PageSize = pageSize };
            var contents = await _baseService.FindListAsync();
            if (status != null) contents = contents.Where(c => c.Status == status);
            if (!string.IsNullOrEmpty(title)) contents = contents.Where(c => c.Title.Contains(title));
            if (!string.IsNullOrEmpty(inputer)) contents = contents.Where(c => c.Inputer.Contains(inputer)); if (contentId > 0) contents = contents.Where(c => c.ContentId == contentId);
            if (categoryId > 0) contents = contents.Where(c => c.CategoryId == categoryId);
            paging.Total = await contents.CountAsync();
            switch (orderType)
            {
                case ContentOrder.IdAsc:
                    contents = contents.OrderBy(c => c.ContentId);
                    break;
                case ContentOrder.IdDesc:
                    contents = contents.OrderByDescending(c => c.ContentId);
                    break;
                case ContentOrder.UpdatedAsc:
                    contents = contents.OrderBy(c => c.CreateTime);
                    break;
                case ContentOrder.UpdatedDesc:
                    contents = contents.OrderByDescending(c => c.CreateTime);
                    break;
                case ContentOrder.HitsAsc:
                    contents = contents.OrderBy(c => c.Hits);
                    break;
                case ContentOrder.HitsDesc:
                    contents = contents.OrderByDescending(c => c.Hits);
                    break;
            }
            paging.Entities = await contents.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToListAsync();
            return paging;
        }
    }
}
