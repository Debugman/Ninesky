﻿/*======================================
 作者：洞庭夕照
 创建：2017.04.18
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 论坛：bbs.ninesky.cn
 版本：v1.0.0.0
 =====================================*/
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Models
{
    /// <summary>
    /// 文章模型
    /// </summary>
    public class Article
    {
        [Key]
        public int ArticleId { get; set; }
        
        /// <summary>
        /// 内容Id
        /// </summary>
        public int ContentId { get; set; }

        /// <summary>
        ///作者 
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "作者")]
        public string Author { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "来源")]
        public string Source { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "关键字")]
        public string Keyword { get; set; }
        
        /// <summary>
        /// 摘要
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "摘要")]
        public string Intro { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Display(Name = "文章内容")]
        public string ArticleContent { get; set; }

        /// <summary>
        /// 内容模型
        /// </summary>
        public Content Content { get; set; }
    }
}
